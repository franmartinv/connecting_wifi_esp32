#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include <tcpip_adapter.h>

#define SSID "FRANCHO"
#define PASSWORD "elputofrancho"

#ifdef __cplusplus
extern "C" {
#endif

void app_main() {
	nvs_flash_init(); //Inicializo escritura en la flash del esp32
	tcpip_adapter_init(); //Inicializo adaptador tcpip para poder conectarme a wifi

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM)); //Lo meto para almacenar la informaci�n de conexi�n
	//en alg�n sitio
	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA)); //Configuro esp32 como estaci�n (para conectarse)

	wifi_config_t staConfig = {
			.sta = {
			.ssid= SSID,
			.password = PASSWORD,
			.bssid_set = false
			}
		};

	ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &staConfig)); //Configuro como estaci�n con los datos
	//dados en la estructura staConfig

	ESP_ERROR_CHECK(esp_wifi_start()); //Inicializo el wifi, sino que mierda vas a mirar tonto que eres tonto
	ESP_ERROR_CHECK(esp_wifi_connect()); //Conecto el wifi de una vez ya
}
